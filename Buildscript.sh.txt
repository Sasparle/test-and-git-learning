#!/bin/bash
echo "Welcome, Sasparle"
firstline=$(head -n1 source/changelog.md)
read -a splitfirstline <<< $firstline
version=${splitfirstline[1]}
echo "You are building version" $version

echo "Enter 1 to continue or 0 to exit"
read versioncontinue

if [ $versioncontinue -eq 1 ]
then
	echo "OK"
  for filename in source/*
  do
    if [ "$filename" == "source/secretinfo.md" ]
    then
    	echo $filename " is NOT being copied"
    else
    	echo $filename " is being copied"
      cp $filename build/
    fi
  done
  echo "Those files are in the build directory: " $(ls build/) " for version " $version
else
	echo "Please come back when you are ready!"
fi

